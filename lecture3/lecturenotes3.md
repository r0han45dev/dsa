### Function Declaration:

- In Python, you can declare a function using the def keyword.
  1. def add_numbers(a, b):
     return a + b

### Function with Default Arguments:

1. def greet(name="Guest"):
   return f"Hello, {name}!"

### Function with Keyword Arguments:

You can pass arguments to a function using keyword names.

1. eg Function with Keyword Arguments:
   You can pass arguments to a function using keyword names.





- javascript

### Function Declaration:
eg. function addNumbers(a, b) {
    return a + b;
    }

### Function Expression:
eg. const greating = function(name){
    return `Hello, ${name}!`;
    }


### Arrow Function:

eg const multiply = (a,b) => return a*b