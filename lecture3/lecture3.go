package lecture3

import "fmt"

type Fullname struct {
	Name    string
	Surname string
}

func Lecture3() {

	var num int = 3
	// switch case
	for i := 1; i <= num; i++ {
		switch i {
		case i % 2:
			fmt.Printf("%v even number \n", i)
		default:
			fmt.Printf("%v odd number \n", i)
		}
	}

	// function and methods
	name := Fullname{Name: "rohan", Surname: "vishwakr"}
	name.Wholename()

}

// method
func (fullname Fullname) Wholename() string {
	var storename Fullname
	storename.Name = fullname.Name
	storename.Surname = fullname.Surname

	return fmt.Sprintf("your name is %s - %s", storename.Name, storename.Surname)
}
