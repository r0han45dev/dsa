# switch case

def switcherCase(day):
    switcher = {
        "Monday": "It's Monday!",
        "Tuesday": "It's Tuesday!",
        "Wednesday": "It's Wednesday!",
        "Thursday": "It's Thursday!",
        "Friday": "It's Friday!",
        "Saturday": "It's Saturday!",
        "Sunday": "It's Sunday!"
    }
    return switcher.get(day,"invalid daya")

day = "Sunday"
print(switcherCase(day))




# function in python and different ways to 

def functionname():
    print("defualt function")

def functionwitharg(name):
    print("name argument function")

def functionwithvariablearr(*args):
    print(args)


def functionwithkeypair(*kwargs):
    for key, value in kwargs.items():
        print(f"{key}: {value}")


