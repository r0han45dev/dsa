
// function declartion 
function addNumbers(a, b) {
    return a + b;
}


// function expresion
const greet = function(name) {
    return `Hello, ${name}!`;
};


// arrow function 
const multiply = (a, b) => a * b;


// defualt para
function greet(name = "Guest") {
    return `Hello, ${name}!`;
}
