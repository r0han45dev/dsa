package lecture2

import "fmt"

func Lecture2() {

	var age int = 18
	if age > 18 {
		fmt.Println("adult")
	}

	for i := 0; i < 5; i++ {
		fmt.Print(i)
	}

	number := []int{1, 34, 4, 5}
	for _, vla := range number {
		fmt.Println(vla)
	}
}
