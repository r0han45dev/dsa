// condition 

let age = 10
if (age > 10) {
    console.log("adult")
} else {
    console.log("minor")
}


// loop 

// take a example u want to loop a give number
let num = 5
for (let i = 0; i < num; i++) {
    console.log(i)
}

// types of loop

// 1] for in :- it use for iterating the key of object
const person = { name: "rohan", age: 30 }
for (const key in person) {
    console.log(person[key])
}

// 2] for of

const person2 = ['conder', 'nicns']
for (const value of person2) {
    console.log(value)
}


// 3] foreach
const person3 = ['conder', 'nicns', 'fa']
person3.forEach((item) => {
    console.log(item)
})


// 4] while loop
// let i = 1
// while (i < 10) {
//     console.log(i)
// }


