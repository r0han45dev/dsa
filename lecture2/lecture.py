age = 18

if age > 18:
    print("adult")
else:
    print("minor")


# normal for loop 
num = [1,3]
for i in num:
    print(i)


# for loop using range
for i in range(5):
    print(i)


# for loop using enumerate
fruit = ['a','v','s']
for index,value in enumerate(fruit):
    print(index,value)


# for loop using zip :- which is use to merge two arr
name = ['a','b']
age = [1,3]

for name,age in zip(name,age):
    print(name,age)



# iniline loop
nums = [1,3,4,5,6]
odd = [num for num in nums if num % 2 !=0 ] 
# print('odd',odd)




# for i in range(4):
#     for j in range(4):
#         print(j+1, end='')
#     print("")

# for i in range(4):
#     for j in range(4):
#         print(4-j+1-1, end='')
#     print('')

count = 0
for i in range(4):
    for j in range(4):
        count = count +1
        print(count, end=',')
    print()
    


for i in range(4):
    for j in range(i+1):
        print("x",end='')
    print()


for i in range(4):
    for j in range(i+1):
        print(i+1,end='')
    print()


count = 0
for i in range(4):
    for j in range(i+1):
        count += 1
        print(count,end='')
    print()


for i in range(4):
    for j in range(i+1):
        print(i-j+1,end='')
    print()
