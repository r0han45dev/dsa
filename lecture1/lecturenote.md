## FlowChart and pseudo code.

#### find Avg of 3 number

- read three number and store the value of three number in a,b and c
- calculate the sum of three number by adding the number
- Avg = sum of three number/2
- print the avg value

#### fing GCD of 12,6

- read two number a and b
- check if a > b if not then swap the a and b using temp = 0
- create loop to check if b value is equal to 0 or not

#### Data types

- numeric
- boolean
- float
- string
- Array
- Object - Dictory

#### Variables

- Go

  1. var datatype nameofvariable = value eg:- var int number = 1
  2. name:= value eg number:=1
  3. const nameofvariable = value eg:- const number = i

- py

  1. single variable declaration eg:- num =1
  2. multiple variable declaration eg:- a,b,c = 1,2,3
  3. swap variable eg:- x,y = 10,20 x,y= y,z

- js 

  1. single variable declaration eg:- let num =1
  2. multiple variable declaration eg:- let a=1,b=2,c = 2
  3. swap variable eg:- let x= 10,x = 20 [x,y]=[y,x]
