package lecture1

import "fmt"

const value = "0"

func Lecture1() {
	fmt.Print("lecture one")

	// variable declartion
	var number int = 0

	number2 := 0

	fmt.Println(number, number2, value)

	// swap number without using temp
	var a, b int = 10, 20

	fmt.Printf("before swapping number  a is %v and b is %v", a, b)
	a, b = b, a
	fmt.Println()

	fmt.Printf("after swapping number  a is %v and b is %v", a, b)

}
