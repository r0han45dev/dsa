// flow char and pseudo code

// GCD of 12,6


function calculateGCD(a, b) {
    let temp = 0

    if (a < b) {
        temp = a
        a = b
        b = temp
    }

    while (b != 0) {
        let mod = a % b
        a = b
        b = mod
    }
    return a
}

console.log(calculateGCD(12, 0))



// single variable
let num = 1
var num2 = 2
const num2 = 3

// mulitple variable
let a = 0, b = 2, c = 4

// sswpa number 
let x = 1, y = 2
[x, y] = [y, x]
